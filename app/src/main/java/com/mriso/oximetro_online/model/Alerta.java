package com.mriso.oximetro_online.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Alerta extends RealmObject {
    @PrimaryKey
    private String idAlerta;
    private String gestorId;
    private String pacienteRelacionado;
    private String textoAlerta;
    private String timeStamp;
    private String respuesta;
    private String timestampRespuesta;
    private Boolean cerrada;

    public Alerta() {
    }

    public String getIdAlerta() {
        return idAlerta;
    }

    public void setIdAlerta(String idAlerta) {
        this.idAlerta = idAlerta;
    }

    public String getGestorId() {
        return gestorId;
    }

    public void setGestorId(String gestorId) {
        this.gestorId = gestorId;
    }

    public String getPacienteRelacionado() {
        return pacienteRelacionado;
    }

    public void setPacienteRelacionado(String pacienteRelacionado) {
        this.pacienteRelacionado = pacienteRelacionado;
    }

    public String getTextoAlerta() {
        return textoAlerta;
    }

    public void setTextoAlerta(String textoAlerta) {
        this.textoAlerta = textoAlerta;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public String getTimestampRespuesta() {
        return timestampRespuesta;
    }

    public void setTimestampRespuesta(String timestampRespuesta) {
        this.timestampRespuesta = timestampRespuesta;
    }

    public Boolean getCerrada() {
        return cerrada;
    }

    public void setCerrada(Boolean cerrada) {
        this.cerrada = cerrada;
    }
}
