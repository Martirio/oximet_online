package com.mriso.oximetro_online.model;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Paciente extends RealmObject {

    @PrimaryKey
    private String documento;

    private String telefono;
    private String nombreCompleto;
    private RealmList<Medicion> listaMediciones;
    private Medicion ultimaMedicion;
    private RealmList<Alerta> listaAlertas;
    private RealmList<String> familiares;
    private String grupoRiesgo;

    public String getGrupoRiesgo() {
        return grupoRiesgo;
    }

    public void setGrupoRiesgo(String grupoRiesgo) {
        this.grupoRiesgo = grupoRiesgo;
    }

    public Paciente() {
    }

    public Paciente(String documento) {
        this.documento = documento;
    }

    public Medicion getUltimaMedicion() {
        return ultimaMedicion;
    }

    public void setUltimaMedicion(Medicion ultimaMedicion) {
        this.ultimaMedicion = ultimaMedicion;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public RealmList<Medicion> getListaMediciones() {
        return listaMediciones;
    }

    public RealmList<Alerta> getListaAlertas() {
        return listaAlertas;
    }

    public void setListaAlertas(RealmList<Alerta> listaAlertas) {
        this.listaAlertas = listaAlertas;
    }

    public RealmList<String> getFamiliares() {
        return familiares;
    }

    public void setFamiliares(RealmList<String> familiares) {
        this.familiares = familiares;
    }

    public void setListaMediciones(RealmList<Medicion> listaMediciones) {
        this.listaMediciones = listaMediciones;

    }
}
