package com.mriso.oximetro_online.fragments;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.clans.fab.FloatingActionButton;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.adapter.Adapter_Mediciones_Main;
import com.mriso.oximetro_online.model.Medicion;
import com.mriso.oximetro_online.utils.func;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Grafico_Fragment extends Fragment {

    public static final String IDPACIENTE = "IDPACIENTE";
    public static final String TEXTOALERTA = "TEXTOALERTA";
    public static final String IDALERTA = "IDALERTA";
    public static final String TIMESTAMPALERTA = "TIMESTAMPALERTA";
    public static final String RESPUESTAALERTA = "RESPUESTAALERTA";
    public static final String NOMBREPACIENTE = "NOMBREPACIENTE";

    private String idPaciente;
    private Button btn_temp;
    private Button btn_oxi;
    private Button btn_puls;
    private LineChart chart;
    private Context fContext;
    private List<ILineDataSet> dataSets;
    private String ultimoGrafico="oxi";
    private LineData laData;
    private Query mediQuery;
    private ValueEventListener valueEventListener;
    private RealmList<Medicion> listaMedicionesOriginales;
    private Adapter_Mediciones_Main adapter;
    private Medicion medicionEvaluada;
    private String timestampAlerta;
    private String idAlerta;
    private String textoAlerta;
    private EditText editRespuesta;
    private TextInputLayout til1;
    private String respuestaAlerta;
    private Cerrable cerrable;
    private String nombrePaciente;
    private String grupoRiesgo;

    public interface Cerrable{
       void cerrarGraficoFragment();
    }

    public Grafico_Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_grafico, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            idPaciente = bundle.getString(IDPACIENTE);
            nombrePaciente=bundle.getString(NOMBREPACIENTE);
            textoAlerta=bundle.getString(TEXTOALERTA);
            idAlerta=bundle.getString(IDALERTA);
            timestampAlerta=bundle.getString(TIMESTAMPALERTA);
            respuestaAlerta=bundle.getString(RESPUESTAALERTA);
        }
        if (idAlerta!=null && !idAlerta.isEmpty()){
            //MUESTRO TARJETA DE ALERTA
            LinearLayout linearLayout = view.findViewById(R.id.alerta);
            linearLayout.setVisibility(View.VISIBLE);
            TextView fecha= view.findViewById(R.id.timestampVerAlerta);
            TextView texto= view.findViewById(R.id.textoAlertaVerAlerta);
            til1=view.findViewById(R.id.til_1);
            editRespuesta=view.findViewById(R.id.editRespuesta);
            if (respuestaAlerta!=null && !respuestaAlerta.isEmpty()){
                editRespuesta.setText(respuestaAlerta);
            }

            fecha.setText(func.formatear(timestampAlerta));
            texto.setText(textoAlerta);
            FloatingActionButton fabGuardar=view.findViewById(R.id.fabGuardarRespuesta);
            fabGuardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gurdarRespuesta(v.getContext());
                }
            });
        }

        chart = view.findViewById(R.id.chart);
        btn_temp = view.findViewById(R.id.botonTemp);
        btn_oxi = view.findViewById(R.id.botonOxi);
        btn_puls = view.findViewById(R.id.botonPuls);
        RecyclerView recyclerMisMediciones = view.findViewById(R.id.recyclerTabla);
        TextView nombrePersona = view.findViewById(R.id.nombrePersona);
        TextView docuPersona = view.findViewById(R.id.dcouPersona);
        dataSets=new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        listaMedicionesOriginales = new RealmList<>();
        adapter= new Adapter_Mediciones_Main(view.getContext(),R.layout.detalle_celda_valores);
        adapter.setListaMedicionesOriginales(listaMedicionesOriginales);
        if (nombrePaciente!=null){
            nombrePersona.setText(nombrePaciente);
        }

        String docuCompleto = ("DNI " + idPaciente);
        docuPersona.setText(docuCompleto);
        recyclerMisMediciones.setLayoutManager(layoutManager);
        recyclerMisMediciones.setAdapter(adapter);

        return view;
    }

    private void iniciarDatosFirebase() {
        DatabaseReference mbase = FirebaseDatabase.getInstance().getReference()
                .child(func.FIREBASE_NODO_PACIENTES)
                .child(idPaciente)
               .child(func.FIREBASE_NODO_UMBRALES);
        mbase.keepSynced(true);
        mbase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    grupoRiesgo=(dataSnapshot.getValue(String.class));
                    cargarMedicionesFirebase();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

    }

    private void cargarMedicionesFirebase() {
        mediQuery = FirebaseDatabase.getInstance().getReference()
                .child(func.FIREBASE_NODO_MEDICIONES)
                .child(idPaciente)
                .orderByKey()
                .limitToLast(10);
        if (valueEventListener==null) {
            valueEventListener= new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        listaMedicionesOriginales.clear();
                        for (DataSnapshot mediSnap :
                                dataSnapshot.getChildren()) {
                            medicionEvaluada = new Medicion();
                            medicionEvaluada.setTimestamp(mediSnap.child(func.FIREBASE_MEDICIONES_TIMESTAMP).getValue(String.class));
                            medicionEvaluada.setOxigenacion(mediSnap.child(func.FIREBASE_OXIGENACION).getValue(String.class));
                            medicionEvaluada.setPulsaciones(mediSnap.child(func.FIREBASE_PULSACIONES).getValue(String.class));
                            medicionEvaluada.setTemperatura(mediSnap.child(func.FIREBASE_TEMPERATURA).getValue(String.class));
                            medicionEvaluada.setGrupoRiesgo(grupoRiesgo);
                            listaMedicionesOriginales.add(medicionEvaluada);
                        }
                        //Collections.reverse(listaMedicionesOriginales); //primera medicion primero
                        crearGrafico(listaMedicionesOriginales);
                        adapter.setListaMedicionesOriginales(listaMedicionesOriginales);
                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            };
        }
        mediQuery.addValueEventListener(valueEventListener);
    }



    private void gurdarRespuesta(Context context) {

        DatabaseReference mbase = FirebaseDatabase.getInstance().getReference()
                .child(func.FIREBASE_NODO_ALERTAS)
                .child(func.FIREBASE_NODO_GESTORES)
                .child(BaseApplication.mSharedPreferenceHelper.getDocumentoGestor())
                .child(idAlerta)
        ;
        if (editRespuesta.getText()==null || editRespuesta.getText().toString().isEmpty()){
           til1.setError(context.getResources().getString(R.string.ingreseRespuesta));
           return;
        }
        else{
            til1.setError(null);
        }
        mbase.child(func.FIREBASE_ALERTA_RESPUESTA).setValue(editRespuesta.getText().toString());
        mbase.child(func.FIREBASE_ALERTA_TIEMPORESPUESTA).setValue(String.valueOf(System.currentTimeMillis() / 1000));
        mbase.child(func.FIREBASE_ALERTA_CERRADA).setValue(true);
        cerrable.cerrarGraficoFragment();
    }




    private void crearGrafico(RealmList<Medicion> mediciones) {
        List<Entry> entriesTemp = new ArrayList<>();
        List<Entry> entriesOxi = new ArrayList<>();
        List<Entry> entriesPuls = new ArrayList<>();
        //final List<String> fechas = new ArrayList<>();
        Integer contador = 0;
        for (Medicion laMed :
                mediciones) {

            String o=laMed.getOxigenacion();
            String p=laMed.getPulsaciones();
            String t=laMed.getTemperatura();

            if (o == null || o.isEmpty()) {
                o=("0");
            }
            if (p == null || p.isEmpty()) {
                p=("0");
            }
            if (t == null || t.isEmpty()) {
                t=("0");
            }
            try {
                entriesOxi.add(new Entry((contador + 1) * 1f, (Float.parseFloat(o))));
                entriesTemp.add(new Entry((contador + 1) * 1f, (Float.parseFloat(t))));
                entriesPuls.add(new Entry((contador + 1) * 1f, (Float.parseFloat(p))));
                contador++;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            //fechas.add(convertirUnixTimeStamp(laMed.getTimestamp()));

        }
        final LineDataSet dataSetOxi = new LineDataSet(entriesOxi, "Oxigenacion");
        dataSetOxi.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSetOxi.setColors(ContextCompat.getColor(fContext, R.color.oxiLinea));
        dataSetOxi.setCircleColor(ContextCompat.getColor(fContext, R.color.oxiLinea));
        dataSetOxi.setLineWidth(3);
        dataSetOxi.setFillAlpha(64);
        dataSetOxi.setDrawFilled(true);
        dataSetOxi.setFillColor(ContextCompat.getColor(fContext, R.color.oxiLinea));
        final LineDataSet dataSetTemp = new LineDataSet(entriesTemp, "Temperatura");
        dataSetTemp.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSetTemp.setColors(ContextCompat.getColor(fContext, R.color.tempLinea));
        dataSetTemp.setCircleColor(ContextCompat.getColor(fContext, R.color.tempLinea));
        dataSetTemp.setLineWidth(3);
        dataSetTemp.setFillAlpha(64);
        dataSetTemp.setDrawFilled(true);
        dataSetTemp.setFillColor(ContextCompat.getColor(fContext, R.color.tempLinea));
        final LineDataSet dataSetPuls = new LineDataSet(entriesPuls, "Pulso");
        dataSetPuls.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSetPuls.setColors(ContextCompat.getColor(fContext, R.color.pulsoLinea));
        dataSetPuls.setCircleColor(ContextCompat.getColor(fContext, R.color.pulsoLinea));
        dataSetPuls.setLineWidth(3);
        dataSetPuls.setFillAlpha(64);
        dataSetPuls.setDrawFilled(true);
        dataSetPuls.setFillColor(ContextCompat.getColor(fContext, R.color.pulsoLinea));



        /*
        ValueFormatter valueFormatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                    return fechas.get((int) value);

            }
        };

         */
        chart.getAxisLeft().setTextSize(12);
        chart.getAxisLeft().setAxisLineWidth(1);
        chart.getAxisRight().setEnabled(false);
        chart.getXAxis().setAxisLineWidth(1);
        chart.getXAxis().setAxisLineColor(ContextCompat.getColor(fContext, R.color.grisOscuro));
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setGranularity(1f);
        //chart.getXAxis().setValueFormatter(valueFormatter);
        chart.getDescription().setEnabled(false);
//        chart.getXAxis().setLabelRotationAngle(0);
        switch (ultimoGrafico){
            case "oxi":
                dataSets.clear();
                chart.getAxisLeft().setAxisMinimum(70f);
                chart.getAxisLeft().setAxisMaximum(100f);
                dataSets.add(dataSetOxi);

                break;
            case "puls":
                dataSets.clear();
                chart.getAxisLeft().setAxisMinimum(50f);
                chart.getAxisLeft().setAxisMaximum(170f);
                dataSets.add(dataSetPuls);
                break;
            case "temp":
                dataSets.clear();
                chart.getAxisLeft().setAxisMinimum(35f);
                chart.getAxisLeft().setAxisMaximum(42f);
                dataSets.add(dataSetTemp);
                break;
        }
        laData= new LineData(dataSets);
        Legend l = chart.getLegend();
        l.setEnabled(true);
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setForm(Legend.LegendForm.CIRCLE); // set what type of form/shape should be used
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        chart.setData(laData);
        chart.invalidate();
        btn_oxi.setPaintFlags(btn_oxi.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btn_temp.setPaintFlags(btn_temp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btn_puls.setPaintFlags(btn_puls.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        btn_oxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ultimoGrafico="oxi";
                dataSets.clear();
                dataSets.add(dataSetOxi);
                laData = new LineData(dataSets);
                chart.setData(laData);
                chart.getAxisLeft().setAxisMinimum(70f);
                chart.getAxisLeft().setAxisMaximum(100f);
                chart.invalidate();
            }
        });
        btn_temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ultimoGrafico="temp";
                dataSets.clear();
                dataSets.add(dataSetTemp);
                laData = new LineData(dataSets);
                chart.setData(laData);
                chart.getAxisLeft().setAxisMinimum(35f);
                chart.getAxisLeft().setAxisMaximum(42f);
                chart.invalidate();
            }
        });
        btn_puls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ultimoGrafico="puls";
                dataSets.clear();
                dataSets.add(dataSetPuls);
                laData = new LineData(dataSets);
                chart.setData(laData);
                chart.getAxisLeft().setAxisMinimum(50f);
                chart.getAxisLeft().setAxisMaximum(170f);
                chart.invalidate();
            }
        });
    }



    @Override
    public void onStop() {
        if (mediQuery!=null && valueEventListener!=null)
            mediQuery.removeEventListener(valueEventListener);
        super.onStop();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.fContext = context;
        this.cerrable=(Cerrable) context;
    }

    @Override
    public void onResume() {
        iniciarDatosFirebase();
        super.onResume();
    }
}
