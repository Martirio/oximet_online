package com.mriso.oximetro_online.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.adapter.Adapter_Mediciones_Main;
import com.mriso.oximetro_online.model.Medicion;
import com.mriso.oximetro_online.model.Paciente;
import com.mriso.oximetro_online.utils.func;

import java.util.ArrayList;

import io.reactivex.internal.operators.flowable.FlowableAmb;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Mediciones_Fragment extends Fragment {

    private RealmList<Medicion> listaMedicionesOriginales;
    private RecyclerView recyclerMisPacientes;
    private Adapter_Mediciones_Main adapterMediciones;
    private RealmList<Paciente> listaPacientesGestor;
    private ValueEventListener medicionesListener;
    private DatabaseReference pacientesQuery;

    public Mediciones_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mediciones, container, false);
//      ---RECYCLERVIEW SETTING
        recyclerMisPacientes = view.findViewById(R.id.recyclerMisPacientes);
        adapterMediciones = new Adapter_Mediciones_Main(view.getContext(),R.layout.detalle_celda_valores_mediciones);
        adapterMediciones.setContext(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerMisPacientes.setLayoutManager(layoutManager);
        if (listaMedicionesOriginales==null){
            listaMedicionesOriginales=new RealmList<>();
        }
        adapterMediciones.setListaMedicionesOriginales(listaMedicionesOriginales);
        recyclerMisPacientes.setAdapter(adapterMediciones);

        //cargarMedicionesFirebase();

        return view;
    }

    private void cargarMedicionesFirebase() {
        if (listaPacientesGestor==null){
            listaPacientesGestor=new RealmList<>();
        }

        pacientesQuery =FirebaseDatabase.getInstance().getReference()
                .child(func.FIREBASE_NODO_GESTORES)
                .child(BaseApplication.mSharedPreferenceHelper.getDocumentoGestor())
                .child(func.FIREBASE_GESTOR_LISTAPACIENTES)
        ;
        if (medicionesListener==null) {
            medicionesListener=new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        listaMedicionesOriginales.clear();
                        listaPacientesGestor.clear();
                        for (DataSnapshot snap :
                                dataSnapshot.getChildren()) {
                            final Paciente paciente= new Paciente();
                            paciente.setDocumento(snap.getKey());
                            paciente.setNombreCompleto(snap.getValue(String.class));

                            DatabaseReference wBase = FirebaseDatabase.getInstance().getReference()
                                    .child(func.FIREBASE_NODO_PACIENTES)
                                    .child(paciente.getDocumento())
                                    .child(func.FIREBASE_NODO_UMBRALES);
                            wBase.keepSynced(true);
                            wBase.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        paciente.setGrupoRiesgo(dataSnapshot.getValue(String.class));
                                        listaPacientesGestor.add(paciente);
                                        cargarMedicionesIndividuales(paciente);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });

                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            };
        }
        pacientesQuery.addListenerForSingleValueEvent(medicionesListener);
    }

    private void cargarMedicionesIndividuales(final Paciente paciente) {

            Query queryPacienteIndividual = FirebaseDatabase.getInstance().getReference()
                    .child(func.FIREBASE_NODO_MEDICIONES)
                    .child(paciente.getDocumento())
                    .orderByKey()
                    .limitToLast(4)
                    ;
            queryPacienteIndividual.keepSynced(true);
            queryPacienteIndividual.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){
                        for (DataSnapshot dati :
                                dataSnapshot.getChildren()) {
                            Medicion lamed= new Medicion();
                            lamed.setTimestamp(dati.child("timestamp").getValue(String.class));
                            lamed.setOxigenacion(dati.child("oxigenacion").getValue(String.class));
                            lamed.setPulsaciones(dati.child("pulsaciones").getValue(String.class));
                            lamed.setTemperatura(dati.child("temperatura").getValue(String.class));
                            lamed.setGrupoRiesgo(paciente.getGrupoRiesgo());
                            lamed.setIdPacienteRelacionado(dataSnapshot.getKey());
                            listaMedicionesOriginales.add(lamed);
                        }

                    }
                    adapterMediciones.notifyDataSetChanged();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
    }

    public static Mediciones_Fragment createMedicionesFragment() {
        return new Mediciones_Fragment();
    }

    @Override
    public void onResume() {
        cargarMedicionesFirebase();
        super.onResume();
    }
}
