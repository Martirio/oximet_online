package com.mriso.oximetro_online.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.adapter.Adapter_Alertas;
import com.mriso.oximetro_online.model.Alerta;
import com.mriso.oximetro_online.utils.func;

import java.util.Collections;
import java.util.Objects;

import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Alertas_Fragment extends Fragment {

    public Alertas_Fragment() {
        // Required empty public constructor
    }




    private ValueEventListener valueEventListener;
    private Query miQuery;

    private RealmList<Alerta> listaAlertasRecycler;
    private Adapter_Alertas adapterAlertas;
    private RecyclerView recyclerMisAlertas;
    private String gestorId;
    private Alertable alertable;
    private TextView sinAlertas;

    public static Fragment createListaAlertasFragment() {
        return new Alertas_Fragment();
    }
    public interface Alertable{
        void verAlertaPaciente(Alerta alerta);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_alertas_, container, false);
        sinAlertas=view.findViewById(R.id.sinAlertas);

        listaAlertasRecycler = new RealmList<>();
        recyclerMisAlertas = view.findViewById(R.id.recyclerMisAlertas);
        adapterAlertas = new Adapter_Alertas();
        adapterAlertas.setContext(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerMisAlertas.setLayoutManager(layoutManager);
        adapterAlertas.setListaAlertasOriginales(listaAlertasRecycler);
        View.OnClickListener listenerAlerta = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int posicion = recyclerMisAlertas.getChildAdapterPosition(v);
                RealmList<Alerta> listaAlertas = adapterAlertas.getListaAlertasOriginales();
                Alerta alertaClickeada  = listaAlertas.get(posicion);
                if (alertaClickeada!=null && alertaClickeada.getPacienteRelacionado()!=null) {
                    alertable.verAlertaPaciente(alertaClickeada);
                } else {
                    Toast.makeText(v.getContext(), R.string.ocurrioUnProblema, Toast.LENGTH_SHORT).show();
                }

            }
        };
        adapterAlertas.setListener(listenerAlerta);
        cargarAlertasFirebase();
        return view;
    }
    private void cargarAlertasFirebase() {
        if (miQuery==null || valueEventListener==null) {

            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    listaAlertasRecycler.clear();
                     RealmList<Alerta> alertasRespondidas = new RealmList<>();
                     RealmList<Alerta> alertasPendientes = new RealmList<>();

                    for (DataSnapshot childNode: dataSnapshot.getChildren()) {
                        try {
                            Alerta alerta=childNode.getValue(Alerta.class);
                            assert alerta != null;
                            alerta.setIdAlerta(childNode.getKey());
                            alerta.setGestorId(dataSnapshot.getKey());
                            if (alerta.getCerrada()==null){
                                alerta.setCerrada(false);
                                alertasPendientes.add(alerta);
                            }
                            else{
                                alertasRespondidas.add(alerta);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    Collections.reverse(alertasRespondidas);
                    listaAlertasRecycler.addAll(alertasPendientes);
                    listaAlertasRecycler.addAll(alertasRespondidas);
                    if (listaAlertasRecycler.size()<1){
                        sinAlertas.setVisibility(View.VISIBLE);
                    }
                    else{
                        sinAlertas.setVisibility(View.GONE);
                    }
                    adapterAlertas.notifyDataSetChanged();

                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            };
            miQuery = FirebaseDatabase.getInstance().getReference()
                    .child(func.FIREBASE_NODO_ALERTAS)
                    .child(func.FIREBASE_NODO_GESTORES)
                    .child(BaseApplication.mSharedPreferenceHelper.getDocumentoGestor())
                    .orderByChild(func.FIREBASE_MEDICIONES_TIMESTAMP);
        }

    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.alertable = (Alertable) context;
    }

    @Override
    public void onStart() {

        if (miQuery!=null && valueEventListener!=null) {
            miQuery.addValueEventListener(valueEventListener);
        }

        if (adapterAlertas != null && recyclerMisAlertas != null) {
            recyclerMisAlertas.setAdapter(adapterAlertas);
            adapterAlertas.notifyDataSetChanged();
        }
        super.onStart();
    }

    @Override
    public void onStop() {

        miQuery.removeEventListener(valueEventListener);
        recyclerMisAlertas.setAdapter(null);
        super.onStop();
    }
}
