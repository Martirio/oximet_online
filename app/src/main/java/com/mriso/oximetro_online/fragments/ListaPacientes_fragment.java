package com.mriso.oximetro_online.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.adapter.Adapter_Pacientes;
import com.mriso.oximetro_online.model.Medicion;
import com.mriso.oximetro_online.model.Paciente;
import com.mriso.oximetro_online.utils.func;

import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaPacientes_fragment extends Fragment {


    private ValueEventListener mediValueEventListener;
    private ValueEventListener pacientesValueEventListener;
    public ListaPacientes_fragment() {
        // Required empty public constructor
    }

    private Graficable graficable;
    private RealmList<Paciente> listaPacientesRecycler;
    private Adapter_Pacientes adapterPacientes;
    private RecyclerView recyclerMisPacientes;

    public interface Graficable {
        void GraficarDatosPaciente(Paciente unPacienteGraficable);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lista_pacientes_v2, container, false);
        listaPacientesRecycler = new RealmList<>();
//      ---RECYCLERVIEW SETTING
        recyclerMisPacientes = view.findViewById(R.id.recyclerMisPacientes);
        adapterPacientes = new Adapter_Pacientes();
        adapterPacientes.setContext(getContext());
        recyclerMisPacientes.setAdapter(adapterPacientes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerMisPacientes.setLayoutManager(layoutManager);
        adapterPacientes.setListaPacientesOriginales(listaPacientesRecycler);
        View.OnClickListener listenerPaciente = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int posicion = recyclerMisPacientes.getChildAdapterPosition(v);
                RealmList<Paciente> listaAuditorias = adapterPacientes.getListaPacientesOriginales();
                Paciente pacienteClickeado = listaAuditorias.get(posicion);
                graficable.GraficarDatosPaciente(pacienteClickeado);
            }
        };
        adapterPacientes.setListener(listenerPaciente);
//      RECYCLERVIEW SETTING---

        return view;
    }



    private void cargarPacientesDesdeFirebase() {
        Query miPacientesQuery = FirebaseDatabase.getInstance().getReference()
                .child(func.FIREBASE_NODO_GESTORES)
                .child(BaseApplication.mSharedPreferenceHelper.getDocumentoGestor())
                .child(func.FIREBASE_GESTOR_LISTAPACIENTES);
        miPacientesQuery.keepSynced(true);

       if (pacientesValueEventListener==null ){
           pacientesValueEventListener = new ValueEventListener() {
               @Override
               public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                   listaPacientesRecycler.clear();
                   if (dataSnapshot.exists()) {
                       for (DataSnapshot snap :
                               dataSnapshot.getChildren()) {
                           final Paciente paciente= new Paciente();
                           paciente.setDocumento(snap.getKey());
                           paciente.setNombreCompleto(snap.getValue(String.class));


                           DatabaseReference wBase = FirebaseDatabase.getInstance().getReference()
                                   .child(func.FIREBASE_NODO_PACIENTES)
                                   .child(paciente.getDocumento())
                                   .child(func.FIREBASE_NODO_UMBRALES);
                           wBase.keepSynced(true);
                           wBase.addListenerForSingleValueEvent(new ValueEventListener() {
                               @Override
                               public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                   if (dataSnapshot.exists()) {
                                       paciente.setGrupoRiesgo(dataSnapshot.getValue(String.class));
                                       listaPacientesRecycler.add(paciente);
                                   }
                                   adapterPacientes.notifyDataSetChanged();
                                   cargarMedicionesFirebase();
                               }

                               @Override
                               public void onCancelled(@NonNull DatabaseError databaseError) {
                               }
                           });

                       }

                   }
               }

               @Override
               public void onCancelled(@NonNull DatabaseError databaseError) {
               }
           };
       }
        miPacientesQuery.addListenerForSingleValueEvent(pacientesValueEventListener);
    }

    private void cargarMedicionesFirebase() {
        if (mediValueEventListener==null) {
            mediValueEventListener=new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Medicion lamed= new Medicion();
                        for (DataSnapshot dati :
                                dataSnapshot.getChildren()) {
                                lamed.setTimestamp(dati.child("timestamp").getValue(String.class));
                                lamed.setOxigenacion(dati.child("oxigenacion").getValue(String.class));
                                lamed.setPulsaciones(dati.child("pulsaciones").getValue(String.class));
                                lamed.setTemperatura(dati.child("temperatura").getValue(String.class));
                                lamed.setIdPacienteRelacionado(dataSnapshot.getKey());
                        }
                        for (Paciente elpa :
                                listaPacientesRecycler) {
                            if (elpa.getDocumento().equals(lamed.getIdPacienteRelacionado())){
                                elpa.setUltimaMedicion(lamed);
                            }
                        }
                        adapterPacientes.notifyDataSetChanged();
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            };
        }
        for (Paciente elPaciente :
                listaPacientesRecycler) {
            Query laque = FirebaseDatabase.getInstance().getReference()
                    .child(func.FIREBASE_NODO_MEDICIONES)
                    .child(elPaciente.getDocumento())
                    .orderByKey()
                    .limitToLast(1)
                    ;
            laque.keepSynced(true);
            laque.addListenerForSingleValueEvent(mediValueEventListener);
        }
    }

    /*
     Query queryMed = FirebaseDatabase.getInstance().getReference().child(MEDICIONES).child(paciente.getDocumento()).orderByChild(func.FIREBASE_MEDICIONES_TIMESTAMP).limitToLast(1);
                       queryMed.addListenerForSingleValueEvent(new ValueEventListener() {
                           @Override
                           public void onDataChange(@NonNull DataSnapshot dataSnapshotMed) {
                               Medicion lamed = dataSnapshotMed.getValue(Medicion.class);
                           }

                           @Override
                           public void onCancelled(@NonNull DatabaseError databaseError) {
                           }
                       });
     */


    public static ListaPacientes_fragment createListaPacientesFragment() {
        return new ListaPacientes_fragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.graficable = (Graficable) context;
    }

    @Override
    public void onResume() {
        cargarPacientesDesdeFirebase();
        super.onResume();
    }


}
