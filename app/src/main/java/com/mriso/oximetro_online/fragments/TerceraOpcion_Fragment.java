package com.mriso.oximetro_online.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.mriso.oximetro_online.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TerceraOpcion_Fragment extends Fragment {

    public TerceraOpcion_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.tercer_fragment, container, false);
    }

    public static TerceraOpcion_Fragment createSegundaOpcionFragment() {
        return new TerceraOpcion_Fragment();
    }

}
