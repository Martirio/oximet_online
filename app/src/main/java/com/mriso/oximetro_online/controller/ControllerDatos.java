package com.mriso.oximetro_online.controller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elmar on 13/8/2017.
 */

public class ControllerDatos {

    public ControllerDatos() {
    }

    public List<String> traerListaViewPagerMain() {
        List<String> unaLista = new ArrayList<>();
        unaLista.add("Alertas");
        unaLista.add("Pacientes");
        unaLista.add("Mediciones");
         return unaLista;
    }
}
