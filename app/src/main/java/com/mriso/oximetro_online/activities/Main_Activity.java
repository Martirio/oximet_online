package com.mriso.oximetro_online.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.adapter.Adapter_PagerMain;
import com.mriso.oximetro_online.controller.ControllerDatos;
import com.mriso.oximetro_online.fragments.Alertas_Fragment;
import com.mriso.oximetro_online.fragments.ListaPacientes_fragment;
import com.mriso.oximetro_online.model.Alerta;
import com.mriso.oximetro_online.model.Paciente;
import com.mriso.oximetro_online.utils.func;

import pl.tajchert.nammu.Nammu;

public class Main_Activity extends AppCompatActivity implements ListaPacientes_fragment.Graficable, Alertas_Fragment.Alertable {

    public static final String IDUSER = "IDUSER";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
///     --SETEAR ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar));
///     SETEAR ACTION BAR--

        ControllerDatos controllerDatos = new ControllerDatos();

        ViewPager pager = findViewById(R.id.viewPagerMyAudits);
        Adapter_PagerMain adapterPager = new Adapter_PagerMain(getSupportFragmentManager(), controllerDatos.traerListaViewPagerMain());
        pager.setAdapter(adapterPager);
        adapterPager.notifyDataSetChanged();
        TabLayout tabLayout = findViewById(R.id.tabLayoutMain);
        tabLayout.setupWithViewPager(pager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int elid = item.getItemId();
        if (elid == R.id.botonSalir) {
            preguntarSalir();
        }
        if (elid==R.id.cerrarSesion){
            cerrarSesion();
        }
        return true;
    }

    private void cerrarSesion() {
        new MaterialDialog.Builder(Main_Activity.this)
                .contentColor(ContextCompat.getColor(Main_Activity.this, R.color.turquesaoscuro))
                .titleColor(ContextCompat.getColor(Main_Activity.this, R.color.oscuro))
                .title(R.string.atencion)
                .cancelable(false)
                .content(R.string.deseaSalir)
                .positiveText(R.string.si)
                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        try {
                            FirebaseAuth.getInstance().signOut();
                            Intent intent = new Intent(Main_Activity.this, Login_Activity.class);
                            startActivity(intent);
                            Main_Activity.this.finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .show();
    }

    private void preguntarSalir() {
        new MaterialDialog.Builder(Main_Activity.this)
                .contentColor(ContextCompat.getColor(Main_Activity.this, R.color.turquesaoscuro))
                .titleColor(ContextCompat.getColor(Main_Activity.this, R.color.oscuro))
                .title(R.string.quit)
                .content(R.string.des_quit)
                .positiveText(R.string.quit)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Main_Activity.this.finishAffinity();
                    }
                })
                .negativeText(getResources().getString(R.string.cancel))
                .show();
    }

    @Override
    public void onBackPressed() {
        preguntarSalir();
    }

    @Override
    public void GraficarDatosPaciente(Paciente unPacienteGraficable) {
        Intent elIntent = new Intent(Main_Activity.this, Grafico_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Grafico_Activity.IDPACIENTE, unPacienteGraficable.getDocumento());
        bundle.putString(Grafico_Activity.NOMBREPACIENTE, unPacienteGraficable.getNombreCompleto());
        elIntent.putExtras(bundle);
        Main_Activity.this.startActivity(elIntent);
    }

    @Override
    public void verAlertaPaciente(Alerta alerta) {
        Intent elIntent = new Intent(Main_Activity.this, Grafico_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Grafico_Activity.IDPACIENTE, alerta.getPacienteRelacionado());
        bundle.putString(Grafico_Activity.IDALERTA, alerta.getIdAlerta() );
        bundle.putString(Grafico_Activity.TEXTOALERTA,alerta.getTextoAlerta());
        bundle.putString(Grafico_Activity.TIMESTAMPALERTA,alerta.getTimeStamp());
        bundle.putString(Grafico_Activity.RESPUESTAALERTA,alerta.getRespuesta());

        elIntent.putExtras(bundle);
        Main_Activity.this.startActivity(elIntent);
    }
}
