package com.mriso.oximetro_online.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.fragments.Grafico_Fragment;
import com.mriso.oximetro_online.utils.func;

public class Grafico_Activity extends AppCompatActivity implements Grafico_Fragment.Cerrable {

    public static final String IDPACIENTE = "IDPACIENTE";
    public static final String NOMBREPACIENTE = "NOMBREPACIENTE";
    public static final String TEXTOALERTA = "TEXTOALERTA";
    public static final String IDALERTA = "IDALERTA";
    public static final String TIMESTAMPALERTA = "TIMESTAMPALERTA";
    public static final String RESPUESTAALERTA  = "RESPUESTAALERTA";
    private String idPaciente;
    private String idAlerta;
    private String textoAlerta;
    private String timestampalerta;
    private String respuestaAlerta;
    private String nombrePaciente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafico);
        Intent elIntent = getIntent();
        Bundle bundle = elIntent.getExtras();
        if (bundle != null) {
            idPaciente = bundle.getString(IDPACIENTE);
            nombrePaciente = bundle.getString(NOMBREPACIENTE);
            idAlerta = bundle.getString(IDALERTA);
            textoAlerta = bundle.getString(TEXTOALERTA);
            timestampalerta = bundle.getString(TIMESTAMPALERTA);
            respuestaAlerta = bundle.getString(RESPUESTAALERTA);
        }
///     --SETEAR ACTION BAR
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.actionbar));
        actionBar.setDisplayHomeAsUpEnabled(true);
///     SETEAR ACTION BAR--
        cargarFragmentGraficos(idPaciente);

    }

    private void cargarFragmentGraficos(String idPaciente) {
        Grafico_Fragment fragmentGrafico = new Grafico_Fragment();
        Bundle bundle = new Bundle();
        bundle.putString(Grafico_Fragment.IDPACIENTE, idPaciente);
        bundle.putString(Grafico_Fragment.NOMBREPACIENTE, nombrePaciente);
        bundle.putString(Grafico_Fragment.TEXTOALERTA, textoAlerta);
        bundle.putString(Grafico_Fragment.IDALERTA, idAlerta);
        bundle.putString(Grafico_Fragment.TIMESTAMPALERTA, timestampalerta);
        bundle.putString(Grafico_Fragment.RESPUESTAALERTA, respuestaAlerta);
        fragmentGrafico.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.contenedorGraficos, fragmentGrafico, "grafico");
        fragmentTransaction.addToBackStack(func.FRAGMENT_GRAFICOS);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        Grafico_Activity.this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void cerrarGraficoFragment() {
        FragmentManager fManager= getSupportFragmentManager();
        fManager.popBackStackImmediate();
        Grafico_Activity.this.finish();
    }
}
