package com.mriso.oximetro_online.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.mriso.oximetro_online.BuildConfig;
import com.mriso.oximetro_online.R;

public class Splash_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TextView version = findViewById(R.id.versionApp);
        version.setText(BuildConfig.VERSION_NAME);
        int SPLASH_DISPLAY_LENGHT = 1500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(Splash_Activity.this, Login_Activity.class);
                Splash_Activity.this.startActivity(mainIntent);
                Splash_Activity.this.finish();

            }
        }, SPLASH_DISPLAY_LENGHT);

    }
}
