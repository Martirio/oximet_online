package com.mriso.oximetro_online.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.utils.HTTPConnectionManager;
import com.mriso.oximetro_online.utils.func;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login_Activity extends AppCompatActivity {

    private EditText editPass;
    private EditText editUsuario;
    private TextInputLayout til1;
    private TextInputLayout til2;
    private SharedPreferences config;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressBar progressBar;
    private String mail;
   // private Button botonRegister;
    private Button botonLogin;
    private TextView passwordOlvidada;
    private String contrasenia;
    private String documento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
        mAuth = FirebaseAuth.getInstance();
        config = this.getSharedPreferences("prefs", 0);
        progressBar=findViewById(R.id.progressBar);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null && user.getEmail()!=null) {

                    Query mQuery= FirebaseDatabase.getInstance().getReference()
                            .child(func.FIREBASE_NODO_GESTORES)
                            .orderByChild(func.FIREBASE_CORREO)
                            .equalTo(user.getEmail().toLowerCase());
                    mQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()){
                                for (DataSnapshot snapi :
                                        dataSnapshot.getChildren()) {
                                    documento = snapi.getKey();
                                }
                                if (documento!=null){
                                    irAMain(documento);
                                    BaseApplication.mSharedPreferenceHelper.setDocumentoGestor(documento);
                                    progressBar.setVisibility(View.GONE);
                                }

                            }
                            else{
                                progressBar.setVisibility(View.GONE);
                                mensajeUsuarioInexistente();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });


                }
                else{
                    progressBar.setVisibility(View.GONE);
                }
            }
        };
        passwordOlvidada = findViewById(R.id.passwordOlvidada);
        til1 = findViewById(R.id.inputLayout1);
        til2 = findViewById(R.id.inputLayout2);
        botonLogin = findViewById(R.id.buttonLogin);
      //botonRegister=findViewById(R.id.buttonRegister);
        editUsuario = findViewById(R.id.editTextUsuario);
        editPass = findViewById(R.id.editTextPassword);
        botonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager inputManager = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    assert inputManager != null;
                    inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
                til1.setError(null);
                til2.setError(null);
                int validar = 0;
                if (editUsuario.getText().toString().isEmpty()||!isEmailValid(editUsuario.getText().toString())) {
                    til1.setError(getResources().getString(R.string.ingreseUsuarioValido));
                    validar = validar + 1;
                }
                if (editPass.getText().toString().toLowerCase().isEmpty()) {
                    til2.setError(getResources().getString(R.string.ingreseContraseniaValida));
                    validar = validar + 1;
                }
                if (validar > 0) {
                    return;
                }
                mail = editUsuario.getText().toString().trim()
                        .toLowerCase();
                contrasenia = editPass.getText().toString();

                progressBar.setVisibility(View.VISIBLE);
                loguearFirebaseManual(mail, contrasenia);

            }
        });

//        botonRegister.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                SI EL BOTON ESTA INVISIBLE QUE REGISTRE EL USUARIO
//                if (botonLogin.getVisibility() == View.GONE) {
//
//                    Integer control = 0;
//                    til1.setError(null);
//                    til2.setError(null);
//
//                    if (editUsuario.getText().toString().isEmpty() || !isEmailValid(editUsuario.getText().toString())) {
//                        til1.setError(getResources().getString(R.string.ingreseUsuarioValido));
//                        control = control + 1;
//                    }
//
//                    if (editPass.getText().toString().isEmpty()) {
//                        til2.setError(getResources().getString(R.string.ingreseContraseniaValida));
//                        control = control + 1;
//                    }
//                    if (editPass.getText().toString().length() < 6) {
//                        til2.setError(getResources().getString(R.string.caracteresContrasenia));
//                        control = control + 1;
//                    }
//
//                    if (control > 0) {
//                        return;
//                    }
//
//                    if (HTTPConnectionManager.isNetworkingOnline(Login_Activity.this)) {
//                        progressBar.setVisibility(View.VISIBLE);
//                        progressBar.setIndeterminate(true);
//                        String usuario = editUsuario.getText().toString().trim();
//                        String pass = editPass.getText().toString();
//
//                        crearCuentaFirebase(usuario, pass);
//                    } else {
//                        Snackbar.make(editUsuario, R.string.noHayInternet, Snackbar.LENGTH_LONG)
//                                .setAction(R.string.reintentar, new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        botonRegister.performClick();
//                                    }
//                                })
//                                .show();
//                    }
//
//                } else {
//
//                    botonLogin.setVisibility(View.GONE);
//                    passwordOlvidada.setVisibility(View.GONE);
//                    editPass.setText(null);
//                    editUsuario.setText(null);
//
//
//                }
//            }
//        });

        passwordOlvidada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!editUsuario.getText().toString().isEmpty() && isEmailValid(editUsuario.getText().toString())){
                    reestablecerContrasenia();
                }
                else{
                    til1.setError(getResources().getString(R.string.ingreseUsuarioValido));
                    new MaterialDialog.Builder(Login_Activity.this)
                            .contentColor(ContextCompat.getColor(Login_Activity.this, R.color.oscuro))
                            .titleColor(ContextCompat.getColor(Login_Activity.this, R.color.turquesaoscuro))
                            .title(R.string.matricula_ingresar)
                            .cancelable(true)
                            .content(getResources().getString(R.string.texto_matricula_ingresar))
                            .positiveText(R.string.ok)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                }
                            })
                            .show();
                }
            }
        });
    }

    private void mensajeUsuarioInexistente() {
        Toast.makeText(this, "El email, no corresponde a un usario activo.", Toast.LENGTH_SHORT).show();
    }

    public void irAMain(String elDocumento) {

        obtenerFCM_Token();
        Intent intent = new Intent(this, Main_Activity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Main_Activity.IDUSER, elDocumento);
        intent.putExtras(bundle);
        startActivity(intent);
        Toast.makeText(this, getResources().getString(R.string.bienvenido), Toast.LENGTH_SHORT).show();
        Login_Activity.this.finish();
    }

    private void obtenerFCM_Token() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            //ESTE DISPOSITIVO NO RECIBIRA NOTIFICACIONES
                            return;
                        }

                        // Get new Instance ID token

                        if (task.getResult()!=null) {
                           guardarTokenFirebase(task.getResult().getToken());
                        }

                    }
                });
    }

    private void guardarTokenFirebase(String token) {
        DatabaseReference mbase = FirebaseDatabase.getInstance().getReference()
                .child(func.FIREBASE_NODO_GESTORES)
                .child(BaseApplication.mSharedPreferenceHelper.getDocumentoGestor())
                .child(func.FIREBASE_GESTOR_TOKEN);
        mbase.setValue(token);
    }
    public void loguearFirebaseManual(final String usuario, final String pass) {

        mAuth.signInWithEmailAndPassword(usuario, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If sign in fails, display a message to the user. If sign in succeeds
                        if (!task.isSuccessful()) {

                            if (HTTPConnectionManager.isNetworkingOnline(Login_Activity.this)) {

                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                    Toast.makeText(Login_Activity.this, getResources().getString(R.string.datosErroneos), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(Login_Activity.this, getResources().getString(R.string.autenticacionFallida), Toast.LENGTH_SHORT).show();
                                }



                            } else {
                                Snackbar.make(editUsuario, R.string.noHayInternetlogin, Snackbar.LENGTH_LONG)
                                        .setAction(R.string.reintentar, new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                loguearFirebaseManual(usuario, pass);
                                            }
                                        })
                                        .show();
                            }
                            progressBar.setVisibility(View.GONE);

                        } else {
                            til1.setError(null);
                            til2.setError(null);

                        }

                    }
                });
    }

//    public void crearCuentaFirebase(final String email, final String password) {
//        mAuth.createUserWithEmailAndPassword(email, password)
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//
//                        if (task.isSuccessful()) {
//                            Toast.makeText(Login_Activity.this, R.string.cuentaCreadaFirebase, Toast.LENGTH_SHORT).show();
//                            guardarUsuarioDatabase(mAuth.getCurrentUser());
//                        } else {
//
//                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {
//                                Toast.makeText(Login_Activity.this, getResources().getString(R.string.usuarioRepetido), Toast.LENGTH_LONG).show();
//                            } else {
//                                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
//                                    Toast.makeText(Login_Activity.this, getResources().getString(R.string.datosErroneos), Toast.LENGTH_LONG).show();
//                                } else {
//                                    Snackbar.make(editUsuario, R.string.noHayInternetlogin, Snackbar.LENGTH_LONG)
//                                            .setAction(R.string.reintentar, new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View view) {
//                                                    crearCuentaFirebase(email, password);
//                                                }
//                                            })
//                                            .show();
//
//                                }
//                            }
//                        }
//                        progressBar.setVisibility(View.GONE);
//                    }
//                });
//
//    }

    private void guardarUsuarioDatabase(FirebaseUser user) {
        DatabaseReference mbase=FirebaseDatabase.getInstance().getReference().child(func.FIREBASE_NODO_GESTORES);
        mbase.child(user.getUid()).child(func.FIREBASE_GESTOR_MAIL).setValue(user.getEmail());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }






    public void reestablecerContrasenia() {
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);
        mail = editUsuario.getText().toString().trim();

        new MaterialDialog.Builder(Login_Activity.this)
                .contentColor(ContextCompat.getColor(Login_Activity.this, R.color.turquesaoscuro))
                .titleColor(ContextCompat.getColor(Login_Activity.this, R.color.oscuro))
                .title(R.string.passwordOlvidada)
                .cancelable(true)
                .content(getResources().getString(R.string.enviaremosInstrucciones) + "\n" + mail + "\n" + getResources().getString(R.string.deseaContinuar))
                .positiveText(R.string.ok)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        mAuth.sendPasswordResetEmail(mail)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(Login_Activity.this, getResources().getString(R.string.mailReestablecimientoEnviado) + mail, Toast.LENGTH_LONG).show();
                                        } else {
                                            if (task.getException().getMessage().equals("There is no user record corresponding to this identifier. The user may have been deleted.")) {

                                                Toast.makeText(Login_Activity.this, task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(Login_Activity.this, "error", Toast.LENGTH_SHORT).show();
                                            }

                                        }
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                    }
                })
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .show();
    }
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}

