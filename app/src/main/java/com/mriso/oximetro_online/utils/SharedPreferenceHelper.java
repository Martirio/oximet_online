package com.mriso.oximetro_online.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.concurrent.TimeUnit;



public class SharedPreferenceHelper
{

    private SharedPreferences appPreferences;
    private SharedPreferences.Editor appPreferencesEditor;
    private Context context;



    public SharedPreferenceHelper(Context context)
    {
        this.context = context;

        appPreferences = context.getSharedPreferences("appPreferences", Context.MODE_PRIVATE);
        appPreferencesEditor = appPreferences.edit();


    }


    public void setDocumentoGestor(String docu) {
        appPreferencesEditor.putString("DOCU",docu);
        appPreferencesEditor.commit();
    }
    public String getDocumentoGestor(){
        return appPreferences.getString("DOCU",null);
    }

    public void guardarWarningOxigeno(String value, String grupo) {
        appPreferencesEditor.putString("oxi_warning_"+grupo,value);
        appPreferencesEditor.commit();
    }
    public void guardarCriticoOxigeno(String value, String grupo) {
        appPreferencesEditor.putString("oxi_critico_"+grupo,value);
        appPreferencesEditor.commit();
    }
    public void guardarWarningPulsaciones(String value, String grupo) {
        appPreferencesEditor.putString("pul_warning_"+grupo,value);
        appPreferencesEditor.commit();
    }
    public void guardarCriticoPulsaciones(String value, String grupo) {
        appPreferencesEditor.putString("pul_critico_"+grupo,value);
        appPreferencesEditor.commit();
    }
    public void guardarWarningTemperatura(String value, String grupo) {
        appPreferencesEditor.putString("tem_warning_"+grupo,value);
        appPreferencesEditor.commit();
    }
    public void guardarCriticoTemperatura(String value, String grupo) {
        appPreferencesEditor.putString("tem_critico_"+grupo,value);
        appPreferencesEditor.commit();
    }

    public String traerWarningOxi(String grupo){
        return appPreferences.getString("oxi_warning_"+grupo,func.warning_oxigenacion);
    }
    public String traerCriticoOxi(String grupo){
        return appPreferences.getString("oxi_critico_"+grupo,func.critico_oxigenacion);
    }
    public String traerWarningTem(String grupo){
        return appPreferences.getString("tem_warning_"+grupo,func.warning_temperatura);
    }
    public String traerCriticoTem(String grupo){
    return appPreferences.getString("tem_critico_"+grupo,func.critico_temperatura);
}
    public String traerWarningPul(String grupo){
        return appPreferences.getString("pul_warning_"+grupo,func.warning_pulsaciones);
    }
    public String traerCriticoPul(String grupo){
        return appPreferences.getString("pul_critico_"+grupo,func.critico_pulsaciones);
    }


}

