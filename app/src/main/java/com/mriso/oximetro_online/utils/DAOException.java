package com.mriso.oximetro_online.utils;

public class DAOException extends Exception {

	public DAOException(String error)
	{
		super(error);
	}
	
}
