package com.mriso.oximetro_online.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class func {
    public final static String FIREBASE_NODO_PACIENTES="pacientes";
    public final static String FIREBASE_NODO_GESTORES="gestores";
    public final static String FIREBASE_NODO_MEDICIONES="mediciones";
    public final static String FIREBASE_PACIENTES_TOKEN="pacienteToken";
    public static final String FIREBASE_GESTOR_TOKEN = "gestorToken";
    public static final String FIREBASE_GESTOR_MAIL = "gestorEmail";
    public static final String FIREBASE_NODO_ALERTAS = "alertas";
    public static final String SHARED_BASE = "prefs";
    public static final String SHARED_FIRSTRUN = "firstRun";
    public static final String FIREBASE_PACIENTES_TELEFONO ="telefono" ;
    public static final String FIREBASE_PACIENTES_NOMBRE ="nombre" ;
    public static final String FIREBASE_NODO_FAMILIARES ="familiares" ;
    public static final String FIREBASE_MEDICIONES_TIMESTAMP = "timestamp";
    public static final String FIREBASE_ALERTA_RESPUESTA = "respuesta";
    public static final String FIREBASE_ALERTA_TIEMPORESPUESTA = "timestampRespuesta";
    public static final String FRAGMENT_GRAFICOS = "FRAGMENTGRAFICOS";
    public static final String FIREBASE_ALERTA_CERRADA = "cerrada";
    public static final String FIREBASE_TRUE = "true";
    public static final String FIREBASE_FALSE = "false";
    public static final String FIREBASE_GESTOR_LISTAPACIENTES = "listaPacientes";
    public static final String FIREBASE_CORREO = "correo";
    public static final String FIREBASE_TAG_FAILED = "failed";
    public static final String FIREBASE_OXIGENACION = "oxigenacion";
    public static final String FIREBASE_PULSACIONES = "pulsaciones";
    public static final String FIREBASE_TEMPERATURA = "temperatura";
    //martirio//

    //GRUPO RIESGO A
    public static final String warning_temperatura="37.0";
    public static final String critico_temperatura="37.5";
    public static final String warning_pulsaciones="100";
    public static final String critico_pulsaciones="90";
    public static final String warning_oxigenacion="100";
    public static final String critico_oxigenacion="90";
    public static final String FIREBASE_NODO_UMBRALES = "grupoRiesgo";
    public static final String FIREBASE_WARNING = "umbralWarning";
    public static final String FIREBASE_CRITICO = "umbralCritico";
    //GRUPO RIESGO B

    //GRUPO RIESGO C

    public static String formatear(String timestampAlerta) {
        long dv = Long.parseLong(timestampAlerta) * 1000;
        Date df = new Date(dv);
        return new SimpleDateFormat("dd/MM/YY hh:mm", Locale.US).format(df);
    }
}
