package com.mriso.oximetro_online;

import android.app.Application;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mriso.oximetro_online.utils.SharedPreferenceHelper;
import com.mriso.oximetro_online.utils.func;

import io.realm.Realm;

public class BaseApplication extends Application {
    public static SharedPreferenceHelper mSharedPreferenceHelper;
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mSharedPreferenceHelper = new SharedPreferenceHelper(this);
        traerUmbralesMedicion();
    }

    private void traerUmbralesMedicion() {
        DatabaseReference mbase= FirebaseDatabase.getInstance().getReference().child(func.FIREBASE_NODO_UMBRALES);
        mbase.keepSynced(true);
        mbase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapy :
                            dataSnapshot.getChildren()) {


                        String wo= snapy.child(func.FIREBASE_NODO_MEDICIONES).child(func.FIREBASE_OXIGENACION).child(func.FIREBASE_WARNING).getValue(String.class);
                        String co= snapy.child(func.FIREBASE_NODO_MEDICIONES).child(func.FIREBASE_OXIGENACION).child(func.FIREBASE_CRITICO).getValue(String.class);
                        String wt= snapy.child(func.FIREBASE_NODO_MEDICIONES).child(func.FIREBASE_TEMPERATURA).child(func.FIREBASE_WARNING).getValue(String.class);
                        String ct= snapy.child(func.FIREBASE_NODO_MEDICIONES).child(func.FIREBASE_TEMPERATURA).child(func.FIREBASE_CRITICO).getValue(String.class);
                        String wp= snapy.child(func.FIREBASE_NODO_MEDICIONES).child(func.FIREBASE_PULSACIONES).child(func.FIREBASE_WARNING).getValue(String.class);
                        String cp= snapy.child(func.FIREBASE_NODO_MEDICIONES).child(func.FIREBASE_PULSACIONES).child(func.FIREBASE_CRITICO).getValue(String.class);
                        if (wo!=null && co!=null && wt!=null && ct!=null && wp!=null && cp!=null){
                            BaseApplication.mSharedPreferenceHelper.guardarWarningOxigeno        (wo,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarCriticoOxigeno        (co,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarWarningTemperatura    (wt,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarCriticoTemperatura    (ct,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarWarningPulsaciones    (wp,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarCriticoPulsaciones    (cp,snapy.getKey());
                        } else {
                            BaseApplication.mSharedPreferenceHelper.guardarWarningOxigeno        (func.warning_oxigenacion,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarCriticoOxigeno        (func.critico_oxigenacion,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarWarningTemperatura    (func.warning_temperatura,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarCriticoTemperatura    (func.critico_temperatura,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarWarningPulsaciones    (func.warning_pulsaciones,snapy.getKey());
                            BaseApplication.mSharedPreferenceHelper.guardarCriticoPulsaciones    (func.critico_pulsaciones,snapy.getKey());
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
}