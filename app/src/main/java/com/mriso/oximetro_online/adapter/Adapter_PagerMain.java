package com.mriso.oximetro_online.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mriso.oximetro_online.fragments.Alertas_Fragment;
import com.mriso.oximetro_online.fragments.ListaPacientes_fragment;
import com.mriso.oximetro_online.fragments.Mediciones_Fragment;
import com.mriso.oximetro_online.fragments.TerceraOpcion_Fragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pablo on 31/5/2017.
 */

public class Adapter_PagerMain extends FragmentStatePagerAdapter {

    //EL ADAPTER NECESITA SIEMPRE UNA LISTA DE FRAGMENTS PARA MOSTRAR
    private List<Fragment> listaFragments;

    private List<String> unaListaTitulos;

    public Adapter_PagerMain(FragmentManager fm, List<String> listaTitulos) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        //INICIALIZO LA LISTA DE FRAGMENT
        unaListaTitulos = listaTitulos;
        listaFragments = new ArrayList<>();

        for (String unString : unaListaTitulos) {
            switch (unString) {
                case "Alertas":
                    listaFragments.add(Alertas_Fragment.createListaAlertasFragment());
                    break;
                case "Pacientes":
                    listaFragments.add(ListaPacientes_fragment.createListaPacientesFragment());
                    break;
                case "Mediciones":
                    listaFragments.add(Mediciones_Fragment.createMedicionesFragment());
                    break;
                case "Opciones":
                    listaFragments.add(TerceraOpcion_Fragment.createSegundaOpcionFragment());
                    break;

            }

        }
        //LE AVISO AL ADAPTER QUE CAMBIO SU LISTA DE FRAGMENTS.
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return listaFragments.get(position);
    }

    @Override
    public int getCount() {
        return listaFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return unaListaTitulos.get(position);
    }

}
