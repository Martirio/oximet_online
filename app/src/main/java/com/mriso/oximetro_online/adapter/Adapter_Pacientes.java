package com.mriso.oximetro_online.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.model.Medicion;
import com.mriso.oximetro_online.model.Paciente;

import java.sql.Struct;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.realm.RealmList;

/**
 * Created by elmar on 18/5/2017.
 */

public class Adapter_Pacientes extends RecyclerView.Adapter implements View.OnClickListener {

    private static final String MEDICIONES = "mediciones";

    private Context context;
    private RealmList<Paciente> listaPacientesOriginales;
    private RealmList<Medicion> listaMediciones;
    private View.OnClickListener listener;
    private Medicion medicionUltima;


    public Adapter_Pacientes() {

    }


    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaPacientesOriginales(RealmList<Paciente> listaPacientesOriginales) {
        this.listaPacientesOriginales = listaPacientesOriginales;
    }


    public RealmList<Paciente> getListaPacientesOriginales() {
        return listaPacientesOriginales;
    }

    //crear vista y viewholder
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;
        viewCelda = layoutInflater.inflate(R.layout.detalle_celda_mis_pacientes_v2, parent, false);
        viewCelda.setOnClickListener(this);

        return new PacienteViewHolder(viewCelda);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Paciente unPaciente = listaPacientesOriginales.get(position);
        PacienteViewHolder pacienteViewHolder = (PacienteViewHolder) holder;
        assert unPaciente != null;
        pacienteViewHolder.cargarPaciente(unPaciente);

    }

    @Override
    public int getItemCount() {
        return listaPacientesOriginales.size();
    }


    public void onClick(View view) {
        listener.onClick(view);
    }





    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private class PacienteViewHolder extends RecyclerView.ViewHolder {

        private TextView valorOxigeno;
        private TextView valorTemperatura;
        private TextView valorPulsaciones;
        private TextView nombrePaciente;
        private TextView fechaMedicion;



        //private TextView textViewTitulo;


        PacienteViewHolder(View itemView) {
            super(itemView);

            valorOxigeno= itemView.findViewById(R.id.valorOxigeno);
            valorTemperatura= itemView.findViewById(R.id.valorTemperatura);
            valorPulsaciones= itemView.findViewById(R.id.valorPulsaciones);
            nombrePaciente=itemView.findViewById(R.id.textoNombrePaciente);
            fechaMedicion=itemView.findViewById(R.id.textoFechaMedicion);
        }

        void cargarPaciente(final Paciente unPaciente) {

            //busco la ultima medicion

                nombrePaciente.setText(unPaciente.getNombreCompleto());
                handleMedicion(unPaciente.getUltimaMedicion(),unPaciente.getGrupoRiesgo());

        }

        private void handleMedicion(Medicion laMedicion, String grupoRiesgo) {
            Integer bpm=null;
            Integer oxi=null;
            Double temp=null;
            if (laMedicion!=null) {
                String eloxi;
                String latemp;
                String laspul;
                String fechu;
                String vv;

                if (laMedicion.getTimestamp() == null || laMedicion.getTimestamp().isEmpty()) {
                    fechaMedicion.setText(R.string.sinFecha);
                } else {
                    fechu = laMedicion.getTimestamp();
                    long dv = Long.parseLong(fechu) * 1000;
                    Date df = new Date(dv);
                    vv = new SimpleDateFormat("dd/MM/YY HH:mm", Locale.US).format(df);
                    fechaMedicion.setText(vv);
                }

                if (laMedicion.getOxigenacion() == null || laMedicion.getOxigenacion().isEmpty()) {
                    eloxi = "-";
                    valorOxigeno.setText(eloxi);
                } else {
                    eloxi = laMedicion.getOxigenacion();
                    oxi = Integer.parseInt(eloxi);
                    valorOxigeno.setText(eloxi);
                }
                if (laMedicion.getPulsaciones() == null || laMedicion.getPulsaciones().isEmpty()) {
                    laspul = "-";
                    valorPulsaciones.setText(laspul);
                } else {
                    laspul = laMedicion.getPulsaciones();
                    bpm = Integer.parseInt(laspul);
                    valorPulsaciones.setText(laspul);
                }
                if (laMedicion.getTemperatura() == null || laMedicion.getTemperatura().isEmpty()) {
                    latemp = "-";
                    valorTemperatura.setText(latemp);
                } else {
                    latemp = laMedicion.getTemperatura();
                    temp = Double.parseDouble(latemp) ;
                    valorTemperatura.setText(String.valueOf(temp));
                }



                //setear colores
                //


                if (!latemp.equals("-")) {
                    if (temp >= Double.parseDouble(BaseApplication.mSharedPreferenceHelper.traerCriticoTem(grupoRiesgo))) {
                        valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                    } else if (temp >= Double.parseDouble(BaseApplication.mSharedPreferenceHelper.traerWarningTem(grupoRiesgo))) {
                        valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                    } else {
                        valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                    }
                }
                else{
                    valorTemperatura.setBackgroundColor(ContextCompat.getColor(context,R.color.semaGris));
                }
                if (!eloxi.equals("-")) {
                    if (oxi < Integer.parseInt( BaseApplication.mSharedPreferenceHelper.traerCriticoOxi(grupoRiesgo))) {
                        valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                    } else if (oxi < Integer.parseInt( BaseApplication.mSharedPreferenceHelper.traerWarningOxi(grupoRiesgo))) {
                        valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                    } else {
                        valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                    }
                }
                else{
                    valorOxigeno.setBackgroundColor(ContextCompat.getColor(context,R.color.semaGris));
                }
                if (!laspul.equals("-")) {
                    if (bpm >=Integer.parseInt(BaseApplication.mSharedPreferenceHelper.traerWarningPul(grupoRiesgo))) {
                        valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                    } else if (bpm >= Integer.parseInt(BaseApplication.mSharedPreferenceHelper.traerCriticoPul(grupoRiesgo))) {
                        valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                    } else {
                        valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                    }
                }
                else{
                    valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context,R.color.semaGris));
                }
            }
        }
    }
}



// Decodes image and scales it to reduce memory consumption