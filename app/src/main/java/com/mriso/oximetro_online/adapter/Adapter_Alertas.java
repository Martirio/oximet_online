package com.mriso.oximetro_online.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.github.clans.fab.FloatingActionButton;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.model.Medicion;
import com.mriso.oximetro_online.model.Alerta;
import com.mriso.oximetro_online.utils.func;

import io.realm.RealmList;

/**
 * Created by elmar on 18/5/2017.
 */

public class Adapter_Alertas extends RecyclerView.Adapter implements View.OnClickListener {


    private Context context;
    private RealmList<Alerta> listaAlertasOriginales;
    private View.OnClickListener listener;


    public Adapter_Alertas() {

    }


    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaAlertasOriginales(RealmList<Alerta> listaAlertasOriginales) {
        this.listaAlertasOriginales = listaAlertasOriginales;
    }


    public RealmList<Alerta> getListaAlertasOriginales() {
        return listaAlertasOriginales;
    }

    //crear vista y viewholder
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;
        viewCelda = layoutInflater.inflate(R.layout.detalle_celda_mis_alertas, parent, false);
        viewCelda.setOnClickListener(this);

        return new AlertaViewHolder(viewCelda);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Alerta unAlerta = listaAlertasOriginales.get(position);
        AlertaViewHolder alertaViewHolder = (AlertaViewHolder) holder;
        assert unAlerta != null;
        alertaViewHolder.cargarAlerta(unAlerta);
    }

    @Override
    public int getItemCount() {
        return listaAlertasOriginales.size();
    }


    public void onClick(View view) {
        listener.onClick(view);
    }





    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private class AlertaViewHolder extends RecyclerView.ViewHolder {

        private TextView                textoAlerta;
        private TextView                fechaAlerta;
        private ImageView               imgAlerta;
        private CardView                tarjeta;




        //private TextView textViewTitulo;


        AlertaViewHolder(View itemView) {
            super(itemView);
            textoAlerta= itemView.findViewById(R.id.textoAlerta);
            fechaAlerta=itemView.findViewById(R.id.fechaAlerta);
            imgAlerta=itemView.findViewById(R.id.imagenAlerta);
            tarjeta=itemView.findViewById(R.id.laTarjeta);


        }

        void cargarAlerta(final Alerta unAlerta) {

            if (unAlerta.getTextoAlerta()!=null){
                textoAlerta.setText(unAlerta.getTextoAlerta());
            }
            if (unAlerta.getTimeStamp()!=null){
                fechaAlerta.setText(func.formatear(unAlerta.getTimeStamp()));
            }
            if (unAlerta.getCerrada()){
                imgAlerta.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_done_turq_24dp));
                tarjeta.setBackgroundColor(context.getResources().getColor(R.color.liviaGris));
            }
            else{
                imgAlerta.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_priority_high_black_24dp));
                tarjeta.setBackgroundColor(context.getResources().getColor(R.color.blanco));
            }


        }


    }


}



// Decodes image and scales it to reduce memory consumption