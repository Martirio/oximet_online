package com.mriso.oximetro_online.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mriso.oximetro_online.BaseApplication;
import com.mriso.oximetro_online.R;
import com.mriso.oximetro_online.model.Medicion;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.realm.RealmList;

/**
 * Created by elmar on 18/5/2017.
 */

public class Adapter_Mediciones_Main extends RecyclerView.Adapter {

    private final int layoutMostrable;
    private Context context;
    private RealmList<Medicion> listaMedicionesOriginales;


    public Adapter_Mediciones_Main(Context context, int detalle_celda_valores) {
        this.context=context;
        this.layoutMostrable=detalle_celda_valores;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setListaMedicionesOriginales(RealmList<Medicion> listaMedicionesOriginales) {
        this.listaMedicionesOriginales = listaMedicionesOriginales;
    }

    //crear vista y viewholder
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View viewCelda;
        viewCelda = layoutInflater.inflate(layoutMostrable, parent, false);
        return new PacienteViewHolder(viewCelda);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Medicion unaMedicion = listaMedicionesOriginales.get(position);
        PacienteViewHolder auditoriasViewHolder = (PacienteViewHolder) holder;
        assert unaMedicion != null;
        auditoriasViewHolder.cargarPaciente(unaMedicion);
    }

    @Override
    public int getItemCount() {
        return listaMedicionesOriginales.size();
    }

    //creo el viewholder que mantiene las referencias
    //de los elementos de la celda

    private class PacienteViewHolder extends RecyclerView.ViewHolder {

        private TextView valorOxigeno;
        private TextView valorTemperatura;
        private TextView valorPulsaciones;
        private TextView fecha;
        private TextView dni;
        private TextView numero;

        PacienteViewHolder(View itemView) {
            super(itemView);
            valorOxigeno = itemView.findViewById(R.id.recycleroxi);
            valorTemperatura = itemView.findViewById(R.id.recyclertemp);
            valorPulsaciones = itemView.findViewById(R.id.recyclerppm);
            fecha = itemView.findViewById(R.id.recyclerFecha);
            dni = itemView.findViewById(R.id.recyclerNombre);
            numero=itemView.findViewById(R.id.recyclerNum);
        }

        void cargarPaciente(Medicion laMedicion) {
            Double temp = null;
            Integer oxi = null;
            Integer bpm = null;
            if (laMedicion != null) {
                try {
                    String eloxi;
                    String latemp;
                    String laspul;
                    String fechu;
                    String vv;
                    if (numero!=null){
                        String posicion=String.valueOf(listaMedicionesOriginales.indexOf(laMedicion)+1);
                        numero.setText(posicion);
                    }
                    if (laMedicion.getOxigenacion() == null || laMedicion.getOxigenacion().isEmpty()) {
                        eloxi = "-";
                        valorOxigeno.setText(eloxi);
                    } else {
                        eloxi = laMedicion.getOxigenacion();
                        oxi = Integer.parseInt(eloxi);
                        valorOxigeno.setText(eloxi);
                    }
                    if (laMedicion.getPulsaciones() == null || laMedicion.getPulsaciones().isEmpty()) {
                        laspul = "-";
                        valorPulsaciones.setText(laspul);
                    } else {
                        laspul = laMedicion.getPulsaciones();
                        bpm = Integer.parseInt(laspul);
                        valorPulsaciones.setText(laspul);
                    }
                    if (laMedicion.getTemperatura() == null || laMedicion.getTemperatura().isEmpty()) {
                        latemp = "-";
                        valorTemperatura.setText(latemp);
                    } else {
                        latemp = laMedicion.getTemperatura();
                        temp = (Double.parseDouble(latemp)) ;
                        valorTemperatura.setText(String.valueOf(temp));
                    }
                    if (laMedicion.getTimestamp() == null || laMedicion.getTimestamp().isEmpty()) {
                        fecha.setText("n/a");
                    } else {
                        fechu = laMedicion.getTimestamp();
                        long dv = Long.parseLong(fechu) * 1000;
                        Date df = new Date(dv);
                        vv = new SimpleDateFormat("dd/MM/YY HH:mm", Locale.US).format(df);
                        fecha.setText(vv);
                    }
                    if (layoutMostrable==R.layout.detalle_celda_valores_mediciones) {
                        dni.setText(laMedicion.getIdPacienteRelacionado());
                    }
                    //colores

                    String grupoRiesgo=laMedicion.getGrupoRiesgo();

                    if (!latemp.equals("-")) {
                        if (temp>=Double.parseDouble( BaseApplication.mSharedPreferenceHelper.traerCriticoTem(grupoRiesgo)) ){
                            valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                        } else if (temp >= Double.parseDouble(BaseApplication.mSharedPreferenceHelper.traerWarningTem(grupoRiesgo))){
                            valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                        } else {
                            valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                        }
                    } else {
                        valorTemperatura.setBackgroundColor(ContextCompat.getColor(context, R.color.semaGris));
                    }
                    if (!eloxi.equals("-")) {
                        if (oxi < Integer.parseInt(BaseApplication.mSharedPreferenceHelper.traerCriticoOxi(grupoRiesgo))) {
                            valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                        } else if (oxi < Integer.parseInt(BaseApplication.mSharedPreferenceHelper.traerWarningOxi(grupoRiesgo))) {
                            valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                        } else {
                            valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                        }
                    } else {
                        valorOxigeno.setBackgroundColor(ContextCompat.getColor(context, R.color.semaGris));
                    }
                    if (!laspul.equals("-")) {
                        if (bpm >= Integer.parseInt(BaseApplication.mSharedPreferenceHelper.traerCriticoPul(grupoRiesgo))) {
                            valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaRojo));
                        } else if (bpm >= Integer.parseInt(BaseApplication.mSharedPreferenceHelper.traerWarningPul(grupoRiesgo))) {
                            valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaAmarillo));
                        } else {
                            valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaVerde));
                        }
                    } else {
                        valorPulsaciones.setBackgroundColor(ContextCompat.getColor(context, R.color.semaGris));
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Log.e("ERROR_MEDICION",e.getLocalizedMessage());
                }
            }
        }
    }
}


    // Decodes image and scales it to reduce memory consumption